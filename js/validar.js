//Funcio para controlar que solo que ingrese Numero en la cantidad de Origen
window.addEventListener("load", function () {
  miformulario.cantidadOrigen.addEventListener("keypress", soloNumeros, false);
});

function soloNumeros(e) {
  var key = window.event ? e.which : e.keyCode;
  if ((key < 46 || key > 57)|| key == 47) {
    e.preventDefault();
  } 
}
//Funcion para presentar y ocultar los Select de acuerdo a la Unidad de Medida Seleccionada
var select = document.getElementById('unidad_Medida');
select.addEventListener('change',
  function () {
    var posicion = this.options[select.selectedIndex];
    if (posicion.value == 0) {
      document.getElementById('medida_Logitud1').style.display = "block";
      document.getElementById('medida_Logitud2').style.display = "block";
      document.getElementById('medida_Peso1').style.display = "none";
      document.getElementById('medida_Peso2').style.display = "none";
    } else {
      document.getElementById('medida_Logitud1').style.display = "none";
      document.getElementById('medida_Logitud2').style.display = "none";
      document.getElementById('medida_Peso1').style.display = "block";
      document.getElementById('medida_Peso2').style.display = "block";
      document.getElementById("medida_Peso2").value = '1';
    }
  });

//                              UNIDAD DE MEDIDA DE LONGITUD
//Funcion para controlar que no se seleccione la misma unidad de medida en el Primer Select de origen y destino
var select_Logitud1 = document.getElementById('medida_Logitud1');
var select_Logitud2 = document.getElementById('medida_Logitud2');
select_Logitud1.addEventListener('change',
  function () {
    var posicion_Logitud1 = this.options[select_Logitud1.selectedIndex];
    var posicion_Logitud2 = this.options[select_Logitud2.selectedIndex];
    var valor_index1 = posicion_Logitud1.value;
    var valor_index2 = posicion_Logitud2.value;
    if (valor_index1 === valor_index2) {
      switch (valor_index1) {
        case "0":
          document.getElementById("medida_Logitud2").value = '1';
          break;
        case "1":
          document.getElementById("medida_Logitud2").value = '2';
          break;
        case "2":
          document.getElementById("medida_Logitud2").value = '3';
          break;
        case "3":
          document.getElementById("medida_Logitud2").value = '0';

      }
    }
  });

//Funcion para controlar que no se seleccione la misma unidad de medida en el Segundo Select de origen y destino
var select_Logitud1 = document.getElementById('medida_Logitud1');
var select_Logitud2 = document.getElementById('medida_Logitud2');
select_Logitud2.addEventListener('change',
  function () {
    var posicion_Logitud1 = this.options[select_Logitud1.selectedIndex];
    var posicion_Logitud2 = this.options[select_Logitud2.selectedIndex];
    var valor_index1 = posicion_Logitud1.value;
    var valor_index2 = posicion_Logitud2.value;
    if (valor_index1 === valor_index2) {
      switch (valor_index2) {
        case "0":
          document.getElementById("medida_Logitud1").value = '1';
          break;
        case "1":
          document.getElementById("medida_Logitud1").value = '2';
          break;
        case "2":
          document.getElementById("medida_Logitud1").value = '3';
          break;
        case "3":
          document.getElementById("medida_Logitud1").value = '0';

      }
    }
  });

//                              UNIDAD DE MEDIDA DE PESO
//Funcion para controlar que no se seleccione la misma unidad de medida en el Primer Select de origen y destino
var select_Peso1 = document.getElementById('medida_Peso1');
var select_Peso2 = document.getElementById('medida_Peso2');
select_Peso1.addEventListener('change',
  function () {
    // var posicion_Peso1 = this.options[select_Peso1.selectedIndex].value;
    // var posicion_Peso2 = this.options[select_Peso2.selectedIndex];
    var valor_index1 = this.options[select_Peso1.selectedIndex].value;
    var valor_index2 = this.options[select_Peso2.selectedIndex].value;
    if (valor_index1 === valor_index2) {
      switch (valor_index1) {
        case "0":
          document.getElementById("medida_Peso2").value = '1';
          break;
        case "1":
          document.getElementById("medida_Peso2").value = '2';
          break;
        case "2":
          document.getElementById("medida_Peso2").value = '0';


      }
    }
  });

//Funcion para controlar que no se seleccione la misma unidad de medida en el Segundo Select de origen y destino
var select_Peso1 = document.getElementById('medida_Peso1');
var select_Peso2 = document.getElementById('medida_Peso2');
select_Peso2.addEventListener('change',
  function () {
    var posicion_Peso1 = this.options[select_Peso1.selectedIndex];
    var posicion_Peso2 = this.options[select_Peso2.selectedIndex];
    var valor_index1 = posicion_Peso1.value;
    var valor_index2 = posicion_Peso2.value;
    if (valor_index1 === valor_index2) {
      switch (valor_index2) {
        case "0":
          document.getElementById("medida_Peso1").value = '1';
          break;
        case "1":
          document.getElementById("medida_Peso1").value = '2';
          break;
        case "2":
          document.getElementById("medida_Peso1").value = '0';
      }
    }
  });