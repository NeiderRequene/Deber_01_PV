<?php
session_start();
if (isset($_SESSION['unidadMedida']) && isset($_SESSION['resultado'])) {
  $unidadMedida = $_SESSION['unidadMedida'];
  $cantidadOrigen = $_SESSION['cantidadOrigen'];
  $unidadOrigen_Longitud = $_SESSION['medida_Logitud1'];
  $unidadDestino_Longitud = $_SESSION['medida_Logitud2'];
  $unidadOrigen_Peso = $_SESSION['medida_Peso1'];
  $unidadDestino_Peso = $_SESSION['medida_Peso2'];
  $resultado = $_SESSION['resultado'];
}
?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->  
  <!--<link rel="stylesheet" href="css/bootstrap.css">-->
  <!-- Bootstrap CDN--> 
  <link rel="stylesheet" href="https://bootswatch.com/4/litera/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <title>Convertidor</title>
</head>

<body>
  <!-- menú de navegación -->

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary navbar-toggleable-sm sticky-top">
    <a class="navbar-brand" href="#">Convertidor</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Servicios</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Contacto</a>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Buscar">
        <button class="btn btn-secondary my-2 my-sm-0" type="submit">Buscar</button>
      </form>
    </div>
</nav>
  <!-- fin menú de navegación -->
  <!--Encabezado-->
  <section class="container my-4 py-3 col-6">
    <h3 class="text-center mb-4">Convertidor de Medidas</h3>
    <form action="calcular.php" method="get" name="miformulario">
      <!--Select para las unidades-->
      <div class="">
        <label class="text-center font-weight-bold">Unidad de Medida</label>  
        <select class="form-control border-primary mb-3" id="unidad_Medida" name="unidadMedida">
          <option value="0" <?php echo isset($unidadMedida) && $unidadMedida == 0 ? 'selected' : '' ?> >Logitud</option>
          <option value="1" <?php echo isset($unidadMedida) && $unidadMedida == 1 ? 'selected' : '' ?>>Peso</option>
        </select>
      </div>
      <!--Cantidad Origen-->
      <div class="card-deck mt-4">
      <div class="card border-primary mb-3"> 
        <div class="card-header bg-transparent border-primary font-weight-bold">Cantidad Origen</div> 
        <div class="card-body"> 
          <input type="text" value="<?php echo isset($cantidadOrigen) ? $cantidadOrigen : '' ?>" name="cantidadOrigen" class="form-control" id="cantidadOrigen" placeholder="" required>
          <div class="form-group mt-3">
              <select class="form-control" style="display: <?php echo isset($unidadMedida) && ($unidadMedida == 1) ? 'none' : 'block' ?>" name="medida_Logitud1" id="medida_Logitud1" >
                <option value="0" <?php echo isset($unidadOrigen_Longitud) && $unidadOrigen_Longitud == 0 ? 'selected' : '' ?> >Metro</option>
                <option value="1" <?php echo isset($unidadOrigen_Longitud) && $unidadOrigen_Longitud == 1 ? 'selected' : '' ?>>Kilometro</option>
                <option value="2" <?php echo isset($unidadOrigen_Longitud) && $unidadOrigen_Longitud == 2 ? 'selected' : '' ?>>Milla</option>
                <option value="3" <?php echo isset($unidadOrigen_Longitud) && $unidadOrigen_Longitud == 3 ? 'selected' : '' ?>>Pie</option>
              </select>
              <select class="form-control" style="display: <?php echo isset($unidadMedida) && ($unidadMedida == 0) ? 'none' : '' ?> <?php echo !isset($unidadMedida) ? 'none' : '' ?> " id="medida_Peso1" name="medida_Peso1" >
                <option value="0" <?php echo isset($unidadOrigen_Peso) && $unidadOrigen_Peso == 0 ? 'selected' : '' ?>>Libra</option>
                <option value="1" <?php echo isset($unidadOrigen_Peso) && $unidadOrigen_Peso == 1 ? 'selected' : '' ?>>Kilogramo</option>
                <option value="2" <?php echo isset($unidadOrigen_Peso) && $unidadOrigen_Peso == 2 ? 'selected' : '' ?>>Onza</option>
              </select>
          </div>
        </div>
      </div>
      <!--Cantidad Destino-->
        <div class="card border-primary mb-3"> 
              <div class="card-header bg-transparent border-primary font-weight-bold">Cantidad Destino</div>  
              <div class="card-body">
                <input type="number" class="form-control" value="<?= isset($resultado) ? $resultado : '' ?>" id="resultado" name="cantidadDestino" readonly>
                <div class="form-group mt-3">
                    <select class="form-control" style="display: <?php echo isset($unidadMedida) && ($unidadMedida == 1) ? 'none' : 'block' ?>" id="medida_Logitud2" name="medida_Logitud2">
                      <option value="0" <?php echo isset($unidadDestino_Longitud) && $unidadDestino_Longitud == 0 ? 'selected' : '' ?> >Metro</option>
                      <option value="1" <?php echo isset($unidadDestino_Longitud) && $unidadDestino_Longitud == 1 ? 'selected' : '' ?> <?php echo !isset($unidadDestino_Longitud) ? 'selected' : '' ?>>Kilometro</option>
                      <option value="2" <?php echo isset($unidadDestino_Longitud) && $unidadDestino_Longitud == 2 ? 'selected' : '' ?>>Milla</option>
                      <option value="3" <?php echo isset($unidadDestino_Longitud) && $unidadDestino_Longitud == 3 ? 'selected' : '' ?>>Pie</option>
                    </select>
                    <select class="form-control" style="display: <?php echo isset($unidadMedida) && ($unidadMedida == 0) ? 'none' : '' ?> <?php echo !isset($unidadMedida) ? 'none' : '' ?>" id="medida_Peso2" name="medida_Peso2">
                      <option value="0" <?php echo isset($unidadDestino_Peso) && $unidadDestino_Peso == 0 ? 'selected' : '' ?>>Libra</option>
                      <option value="1" <?php echo isset($unidadDestino_Peso) && $unidadDestino_Peso == 1 ? 'selected' : '' ?>>Kilogramo</option>
                      <option value="2" <?php echo isset($unidadDestino_Peso) && $unidadDestino_Peso == 2 ? 'selected' : '' ?>>Onza</option>
                    </select>
                </div>
              </div>
        </div>
      </div>
        <!--Boton Enviar-->
        <div class="mt-3">
          <input type="submit" value="Calcular" class="btn btn-primary btn-block">
        </div>   
    </form> 
  </section>

  <!-- Footer -->
  <footer class="container-fluid bg-primary">
    <div class="row py-2">
      <div class="col-12">
        <img src="images/neider1.jpg" alt="" width="70px" height="auto" class="rounded-circle mx-auto d-block mr-3">
        <footer class="text-black text-center display-5">Realizado por: <cite title="Source Title">Requené Estacio Neider</cite></footer>
      </div>
    </div>
  </footer>
  
  <!-- Fin Footer -->

    
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <!-- jQuery first, then Tether, then Bootstrap JS. -->
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
  <script src="js/bootstrap.min.js"></script>
  <!--Script para controlar los Select-->
  <script  src="js/validar.js"></script>
</body>

</html>