<?php
function medidas_logitud($unidadOrigen_Longitud,$unidadDestino_Longitud,$cantidadOrigen){
    switch($unidadOrigen_Longitud){
        case "0":
            switch($unidadDestino_Longitud){
                case "1":
                    return calcular($cantidadOrigen,0.001);
                break;
                case "2":
                    return calcular($cantidadOrigen,0.000621371);
                break;
                case "3":
                    return calcular($cantidadOrigen,3.28084);
                break;
            }
        break;
        case "1":
            switch($unidadDestino_Longitud){
                case "0":
                    return calcular($cantidadOrigen,1000);
                break;
                case "2":
                    return calcular($cantidadOrigen,0.621371);
                break;
                case "3":
                    return calcular($cantidadOrigen,3280.84);
                break;
            }
        break;
        case "2":
            switch($unidadDestino_Longitud){
                case "0":
                    return calcular($cantidadOrigen,1609.34);
                break;
                case "1":
                    return calcular($cantidadOrigen,1.60934);
                break;
                case "3":
                    return calcular($cantidadOrigen,5280);
                break;
            }
        break;
        case "3":
            switch($unidadDestino_Longitud){
                case "0":
                    return calcular($cantidadOrigen,0.3048);
                break;
                case "1":
                    return calcular($cantidadOrigen,0.0003048);
                break;
                case "2":
                    return calcular($cantidadOrigen,0.000189394);
                break;
            }
        break;
    }
}