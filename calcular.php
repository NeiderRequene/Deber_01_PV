<?php

// Capturamos los valores de las variables
$unidadMedida=$_GET['unidadMedida'];
$cantidadOrigen = $_GET['cantidadOrigen'];
$unidadOrigen_Longitud = $_GET['medida_Logitud1'];
$unidadDestino_Longitud = $_GET['medida_Logitud2'];
$unidadOrigen_Peso = $_GET['medida_Peso1'];
$unidadDestino_Peso = $_GET['medida_Peso2'];
$resultado="";

// Se controla el tipo de Unidad de Medida Seleccionado
if($unidadMedida=="0"){
    include_once('medidaLongitud.php');
    $resultado= medidas_logitud($unidadOrigen_Longitud,$unidadDestino_Longitud,$cantidadOrigen);
}else{
    include_once('medidaPeso.php');
    $resultado= medidas_Peso($unidadOrigen_Peso,$unidadDestino_Peso,$cantidadOrigen);
}

//Funcion para calcular
function calcular($origen, $destino){
    return $origen*$destino;
 }
 
session_start();
$_SESSION['unidadMedida'] = $unidadMedida;
$_SESSION['cantidadOrigen'] = $cantidadOrigen;
$_SESSION['medida_Logitud1'] = $unidadOrigen_Longitud;
$_SESSION['medida_Logitud2'] = $unidadDestino_Longitud;
$_SESSION['medida_Peso1'] = $unidadOrigen_Peso;
$_SESSION['medida_Peso2'] = $unidadDestino_Peso;
$_SESSION['resultado'] = $resultado;
header('Location: index.php');